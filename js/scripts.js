$(document).ready(function(){
    $('#mycarousel').carousel({interval:2000});
    $('#carouselButton').click(function(){
        if($('#carouselButton').children('span').hasClass('fa-pause')){
            $('#mycarousel').carousel('pause');
            $('#carouselButton').children('span').removeClass('fa-pause');
            $('#carouselButton').children('span').addClass('fa-play');
        }else{
            $('#mycarousel').carousel('cycle');
            $('#carouselButton').children('span').removeClass('fa-play');
            $('#carouselButton').children('span').addClass('fa-pause');
        }
    });
 });

 // Login modal trigger
 $('#loginBtn').click(function(){
    $('#loginModal').modal({
        show: true,
        focus: true
    });
 });

 $('#loginCloseBtn').click(function(){
    $('#loginModal').modal('hide');
 });

 $('#loginCancelBtn').click(function(){
    $('#loginModal').modal('hide');
 });

 // Reserve Table  modal trigger
 $('#cartBtn').click(function(){
    $('#cartModal').modal({
        show: true,
        focus: true
    });
 });

 $('#cartCloseBtn').click(function(){
    $('#cartModal').modal('hide');
 });
 $('#contshopping').click(function(){
    $('#cartModal').modal('hide');
 });
 $('#checkout').click(function(){
    $('#cartModal').modal('hide');
 });